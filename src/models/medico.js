const mongoose = require("mongoose");
var schema = mongoose.Schema;

var medicoSchema = schema({
	nombre: {
		type: String,
		uppercase: true,
	},
	numero_id: Number,
	ciudad: {
		type: String,
		uppercase: true,
	},
	direccion: {
		type: String,
		uppercase: true,
	},
	telefono: Number,
	email: String,
	matricula_prof: {
		type: String,
		uppercase: true,
	},
	especialidad: {
		type: String,
		uppercase: true,
	},
	citas: [String],
	disponible: [String],
});

const Medico = mongoose.model("tbc_medico", medicoSchema);

module.exports = Medico;
