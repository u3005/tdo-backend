const mongoose = require("mongoose");
var schema = mongoose.Schema;

var citaSchema = schema({
	fecha_cita: {
		type: String,
		uppercase: true,
	},
	fecha_solicitud: {
		type: String,
		uppercase: true,
	},
	_id_paciente: String,
	_id_medico: String,
});

const Cita = mongoose.model("tbc_cita", citaSchema);

module.exports = Cita;
