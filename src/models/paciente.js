const mongoose = require("mongoose");
var schema = mongoose.Schema;

var pacienteSchema = schema({
	nombre: {
		type: String,
		uppercase: true,
	},
	numero_id: Number,
	ciudad: {
		type: String,
		uppercase: true,
	},
	direccion: {
		type: String,
		uppercase: true,
	},
	telefono: Number,
	email: String,
	citas: [String],
});

const Paciente = mongoose.model("tbc_paciente", pacienteSchema);

module.exports = Paciente;
