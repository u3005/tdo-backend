const Paciente = require("../models/paciente");

function createPacientes(req, res) {
	var miPaciente = new Paciente(req.body);
	miPaciente.save((err, result) => {
		if (err) {
			res.status(500).send({ message: err });
		} else {
			if (!result) {
				res.status(404).send({ message: "ERROR" });
			} else {
				res.status(200).send({ message: result });
			}
		}
	});
}

function getPacientes(req, res) {
	var idPaciente = req.params.id;
	if (!idPaciente) {
		var result = Paciente.find({}).sort("nombre");
	} else {
		var result = Paciente.find({ _id: idPaciente }).sort("nombre"); //TODO find by id cita || find by id paciente
	}
	result.exec(function (err, result) {
		if (err) {
			res.status(500).send({ message: err });
		} else {
			if (!result) {
				res.status(404).send({ message: "ERROR" });
			} else {
				res.status(200).send({ message: result });
			}
		}
	});
}

function updatePacientes(req, res) {
	var idPaciente = req.params.id;
	Paciente.findOneAndUpdate({ _id: idPaciente }, req.body, { new: true }, function (err, Paciente) {
		if (err) res.send(err);
		res.json(Paciente);
	});
}

function deletePacientes(req, res) {
	var idPaciente = req.params.id;
	Paciente.findByIdAndDelete(idPaciente, function (err, Paciente) {
		if (err) {
			return res.json(500, {
				message: err,
			});
		}
		return res.json(Paciente);
	});
}

module.exports = {
	createPacientes,
	getPacientes,
	updatePacientes,
	deletePacientes,
};
