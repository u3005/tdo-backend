const Cita = require("../models/cita");

function createCitas(req, res) {
	var miCita = new Cita(req.body);
	miCita.save((err, result) => {
		if (err) {
			res.status(500).send({ message: err });
		} else {
			if (!result) {
				res.status(404).send({ message: "ERROR" });
			} else {
				res.status(200).send({ message: result });
			}
		}
	});
}

function getCitas(req, res) {
	var idCita = req.params.id;
	if (!idCita) {
		var result = Cita.find({}).sort("fecha_cita");
	} else {
		var result = Cita.find({ _id: idCita }).sort("fecha_cita"); //TODO find by id cita || find by id paciente
	}
	result.exec(function (err, result) {
		if (err) {
			res.status(500).send({ message: err });
		} else {
			if (!result) {
				res.status(404).send({ message: "ERROR" });
			} else {
				res.status(200).send({ message: result });
			}
		}
	});
}

function updateCitas(req, res) {
	var idCita = req.params.id;
	Cita.findOneAndUpdate({ _id: idCita }, req.body, { new: true }, function (err, Cita) {
		if (err) res.send(err);
		res.json(Cita);
	});
}

function deleteCitas(req, res) {
	var idCita = req.params.id;
	Cita.findByIdAndDelete(idCita, function (err, Cita) {
		if (err) {
			return res.json(500, {
				message: err,
			});
		}
		return res.json(Cita);
	});
}

module.exports = {
	createCitas,
	getCitas,
	updateCitas,
	deleteCitas,
};
