const Medico = require("../models/medico");

function createMedicos(req, res) {
	var miMedico = new Medico(req.body);
	miMedico.save((err, result) => {
		if (err) {
			res.status(500).send({ message: err });
		} else {
			if (!result) {
				res.status(404).send({ message: "ERROR" });
			} else {
				res.status(200).send({ message: result });
			}
		}
	});
}

function getMedicos(req, res) {
	var idMedico = req.params.id;
	if (!idMedico) {
		var result = Medico.find({}).sort("nombre");
	} else {
		var result = Medico.find({ _id: idMedico }).sort("nombre"); //TODO find by id cita || find by id paciente
	}
	result.exec(function (err, result) {
		if (err) {
			res.status(500).send({ message: err });
		} else {
			if (!result) {
				res.status(404).send({ message: "ERROR" });
			} else {
				res.status(200).send({ message: result });
			}
		}
	});
}

function updateMedicos(req, res) {
	var idMedico = req.params.id;
	Medico.findOneAndUpdate({ _id: idMedico }, req.body, { new: true }, function (err, Medico) {
		if (err) res.send(err);
		res.json(Medico);
	});
}

function deleteMedicos(req, res) {
	var idMedico = req.params.id;
	Medico.findByIdAndDelete(idMedico, function (err, Medico) {
		if (err) {
			return res.json(500, {
				message: err,
			});
		}
		return res.json(Medico);
	});
}

module.exports = {
	createMedicos,
	getMedicos,
	updateMedicos,
	deleteMedicos,
};
