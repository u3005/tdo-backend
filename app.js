var express = require("express");
var app = express();

// var bodyParser = require("body-parser");
//var methodOverride = require("method-override");
// var mongoose = require("mongoose");

app.use(express.json());
app.use(
	express.urlencoded({
		extended: true,
	})
);

//CONFIGURACION DE LA CABECERA y CORS
app.use((req, res, next) => {
	res.header("Access-Control-Allow-Origin", "*");

	res.header(
		"Access-Control-Allow-Headers",
		"Authorization, X-API-KEY, Origin, XRequested-With, Content-Type, Accept, Access-Control-Allow-Methods"
	);

	res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");

	res.header("Allow", "GET,POST,PUT,DELETE,OPTIONS");

	next();
});

//RUTAS
app.use(require("./routers/routers"));

module.exports = app;
