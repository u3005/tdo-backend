const { Router } = require("express");
const router = Router();

var controllerCita = require("../src/controllers/controllerCita");
var controllerMedico = require("../src/controllers/controllerMedico");
var controllerPaciente = require("../src/controllers/controllerPaciente");

router.post("/createCitas", controllerCita.createCitas);
router.get("/getCitas/:id?", controllerCita.getCitas);
router.put("/updateCitas/:id", controllerCita.updateCitas);
router.delete("/deleteCitas/:id", controllerCita.deleteCitas);

router.post("/createMedicos", controllerMedico.createMedicos);
router.get("/getMedicos/:id?", controllerMedico.getMedicos);
router.put("/updateMedicos/:id", controllerMedico.updateMedicos);
router.delete("/deleteMedicos/:id", controllerMedico.deleteMedicos);

router.post("/createPacientes", controllerPaciente.createPacientes);
router.get("/getPacientes/:id?", controllerPaciente.getPacientes);
router.put("/updatePacientes/:id", controllerPaciente.updatePacientes);
router.delete("/deletePacientes/:id", controllerPaciente.deletePacientes);

module.exports = router;
